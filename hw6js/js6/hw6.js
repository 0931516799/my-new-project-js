// Функция-конструктор для создания объектa Human.
function Human(name,secondname,age){
    this.name = name;
    this.secondname = secondname;
    this.age = age;
};
// Создание двух экземпляров типа Human
const a = new Human("Vitia","Ivanov","35");
const f = new Human("Вася","Serit","50");

//Второе задание первой части
function Dona(age){
    this.age = age;
}
const human = new Dona(prompt("Сколько вам лет"));
const dir = new Dona(prompt("Сколько вам лет"));
const arr = []; // объявление массива
arr.push(human.age);
arr.push(dir.age);
arr.sort(function(a,b) {
    return a - b 
});
console.log (arr);

//второй пример
arr[0] = {
    name: "Петя",
    id:"5",
    age: 20
}
    arr[1] = {
    name: "Ваня",
    id:"4",
    age: 25
}
arr[2] = {
    name: "Ded",
    id:"3",  
    age: 18
};
 const a = function(arr) {
        arr.sort((a, b) => a.age > b.age ? 1 : -1);
};
console.log(a);
//второе задание
 // Функция-конструктор для создания объектов Human.

 function Human(name,age) {
    // свойство
    this.firstName = name;// свойства экземпляра
    this.age = age;

    // метод экземпляра
    this.say = function () {
        document.write("My name is " + this.firstName + this.age + "old" + "<br /> ");
    }
}
Human.old = 46; // присвоения свойства конструктора
Human.get = function () {
    return new Human("Vasia",45) // мtтод конструктора.
}
// Создание двух экземпляров типа Human

const h = new Human("Anna",24);
const f = new Human("Вася",30);
h.firstName = "Dana" //
h.age = 40;// присвоения свойства екземпляру

// Вызов метода say() на созданых обьектах. 
h.say();//метод екземпляра
f.say();
Human.say();// метод конструктора

