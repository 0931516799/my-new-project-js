/*
Создайте функцию Calculator, который создаёт объекты с тремя методами:
read() запрашивает два значения при помощи prompt и сохраняет их значение в свойствах объекта.
sum() возвращает сумму введённых свойств.
mul() возвращает произведение введённых свойств.
*/ 
class Calculator {

    constructor(){
        this.num1;
        this.num2;
    }

    read(){
        this.num1 = +prompt("Введите первое значение");
        this.num2 = +prompt("Введите второе значение");
    }

    sum(){
       return this.num1 + this.num2
    }

    mul(){
       return this.num1 * this.num2;
    }
}

const calc = new Calculator();
calc.read();

console.log(calc.sum());
console.log(calc.mul());

