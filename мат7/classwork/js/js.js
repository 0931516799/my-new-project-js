/*Реализуйте класс Worker (Работник), который будет иметь следующие свойства: name (имя), 
surname (фамилия), rate (ставка за день работы), days (количество отработанных дней). 
Также класс должен иметь метод getSalary(), который будет выводить зарплату работника. 
Зарплата - это произведение (умножение) ставки rate на количество отработанных дней days.*/

function Worker(name, surname, rate, days) {
    this.name = name;
    this.surname = surname;
    this.rate = rate;
    this.days = days;
}
Worker.prototype.getSalary = function(){
    return this.days * this.rate;
}
const worker1 = new Worker("Vasya", "Petrenko", "50", "20")
document.write(`Привет, ${worker1.name} ${worker1.surname}! Твоя зарплата ${worker1.getSalary()}`);