/*Создайте приложение секундомер.
* Секундомер будет иметь 3 кнопки "Старт, Стоп, Сброс"
* При нажатии на кнопку стоп фон секундомера должен быть красным, старт - зеленый, сброс - серый * Вывод счётчиков в формате ЧЧ:ММ:СС
* Реализуйте Задание используя синтаксис ES6 и стрелочные функции*/

window.onload = () =>  {
   const btnStart = document.getElementById("start");
   const btnStop = document.getElementById("stop");
   const btnReset = document.getElementById("reset");
   
   const stopwatch = document.querySelector(".container-stopwatch")
  
   btnStart.onclick = () => {
       
         stopwatch.classList.add("green")
         stopwatch.classList.remove("black","red","silver")
        
 }


   btnStop.onclick = () => {
    
    stopwatch.classList.add("red")
    stopwatch.classList.remove("black","green","silver")
}  
    btnReset.onclick = () => {
        stopwatch.classList.add("silver")
        stopwatch.classList.remove("black","green","red")
} 
}


