// Реализовать функцию для создания объекта "пользователь". Написать функцию createNewUser(),
//  которая будет создавать и возвращать объект newUser. При вызове функция должна спросить
//   у вызывающего имя и фамилию. Используя данные, введенные пользователем, создать объект
//    newUser со свойствами firstName и lastName. Добавить в объект newUser метод getLogin(),
//     который будет возвращать первую букву имени пользователя, соединенную с фамилией пользователя,
//      все в нижнем регистре (например, Ivan Kravchenko → ikravchenko). Создать пользователя с
//      помощью функции createNewUser(). Вызвать у пользователя функцию getLogin().
//       Вывести в консоль результат выполнения функции.

// class+
// вызов класса+
// передать в класс имя и фамилию+
// добавить в класс метод getLogin()+
// реализовать функциональность getLogin()-
// Вывод данных
// Из метода getLogin() вывести в консоль контекст

class CreateNewUser {
  constructor(name,lastname,password,birtday) {
    this.name = name;
    this.lastname = lastname;
    this.password = password;
    this.birtday = birtday;
  }
  getLogin() {
    return console.log(this.name[0].toLowerCase() + this.lastname.toLowerCase());
  }
  getPassword() {
    return console.log(this.name[0].toUpperCase() + this.lastname.toLowerCase());
  }
  getAge(){
      const now = new Date();//Текущя дата
      const tod = new Date(now.getDate(),now.getMonth(),now.getFullYear());
      const birtd = new Date(this.birtday);//Дата рождения
      const birtage = new Date(birtd.getDate(),birtd.getMonth(),birtd.getFullYear());
      const age = tod.getFullYear() - birtage.getFullYear();//Возраст = текущий год - год рождения
      return console.log(age);
  }
};
const newUser = new CreateNewUser(
  prompt("Введите имя"),
  prompt("Введите фамилию"),
  prompt("Введите ваш пароль"),
  prompt("Введите дату рождения в фортате дд.мм.гггг")
);
console.log(newUser.getLogin());
console.log(newUser.getAge());
console.log(newUser.getPassword());

// var now = new Date(); //Текущя дата

// var today = new Date(now.getFullYear(), now.getMonth(), now.getDate()); //Т
// console.log(today);
// var dob = new Date(1990, 9, 31); //Дата рождения
// var dobnow = new Date(today.getFullYear(), dob.getMonth(), dob.getDate()); //ДР в текущем году
// var age; //Возраст

// //Возраст = текущий год - год рождения
// age = today.getFullYear() - dob.getFullYear();
// //Если ДР в этом году ещё предстоит, то вычитаем из age один год
// if (today < dobnow) {
//   age = age-1;
// }

// document.write (`Возраст: ${age}`);
