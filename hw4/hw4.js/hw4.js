// первое задание

const arr = [1,3,5];
var nam = arr.map(function(num) {
    return num * 2;
  });
  document.write(nam);

// второе задание
function checkAge(age) {
    if (age > 18) {
      return true;
    } else {
      return confirm('Родители разрешили?');
    }
  };
  checkAge();//вызов функции

  //с использованием оператора ?
function checkAge(age) {
    return (age > 18) ? true : confirm('Родители разрешили?');
  };
  checkAge();//вызов функции

  //с использованием оператора ||
function checkAge(age) {
    return (age > 18) || confirm('Родители разрешили?');
  };
  checkAge();//вызов функции