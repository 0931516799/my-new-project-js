let styles = ["Джаз", "Блюз"]; //создание массива с елементами "Джаз", "Блюз"
document.write(styles);
document.write("<hr/>"); //разделительная линия

let plus = styles.push("Рок-н-рол"); //добавление в конец "Рок-н-рол"
document.write("Добавление последнего елемента" + " " + plus + "<br/>");
document.write(styles);
document.write("<hr/>");

let ret = styles.length / 2;
document.write(Math.floor(ret)); //поиск средины массива
document.write("<hr/>");

styles.splice(1, 1, "Классика"); //замена "Блюз" на "Классика"
document.write(styles);
document.write("<hr/>");

//styles.shift();
document.write(styles.shift() + "<br/>"); // удаленный первый елемент
document.write("Массив с удаленным первым елементом :" + " " + styles); //массив с удаленным первым елементом
document.write("<hr/>");

styles.unshift("Реп", "Регги"); // добавление в начало "Реп","Регги"
document.write(styles);
