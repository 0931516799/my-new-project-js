
// Используя JS cоздайте 5 полей для воода данных и остаилизуйте их Добавьте стили на ошыбку и стиль на верный ввод.
// Поля:
// Имя (Укр. Буквы), Номер телефона в формате +38ХХХ-ХХХ-ХХ-ХХ, електронная почта, пароль и подтверждение пароля (пароли должны совпадать ).

// Реазизуй проверку данных.
// При вводе данных сразу проверять на правильность и выводить ошибку если такая необходима.
// добавь кнопку регистрация, при нажатии кнопки проверить все поля и вывести ошибку если такая будет.

// Сохраниение.
// Если пользователь все записал верно, то сохраните данные на клиенте с указанием даты и времени сохранения.  

// Для стилизации используй CSS классы. Для создания элементов используй JS.



window.addEventListener('DOMContentLoaded', () => {
    const createInput = (type, value = '', className, attr) => {
        // Создание input
        const input = document.createElement('input');

        input.type = type;
        // Создание пользовательского атрибута
        input.setAttribute(attr, '');

        input.placeholder = value;
        input.classList.add(className);
        console.dir(input);
        return input;
    }
    
    const span = document.createElement('span');

    document.body.append(createInput('text', 'введите имя', 'inputName', 'data-name'));

    document.body.append(createInput('tel', 'введите телефон', 'inputName', 'data-tel'));
    document.body.append(span);
    document.body.append(createInput('email', 'введите email', 'inputName', 'data-email'));
    document.body.append(createInput('password', 'введите пароль', 'inputName', 'data-pass-one'));
    document.body.append(createInput('password', 'повторите пароль', 'inputName', 'data-pass-two'));
    document.querySelectorAll('input').forEach(function (e) {
        e.addEventListener('input', (a) => {

            if (typeof e.dataset.name == 'string') {

                if (e.value.search(/[а-яі]/i) < 0) {
                    
                    span.innerHTML = 'Не верно указано имя';
                    e.classList.add('incorrect')

                } else {
                    span.innerHTML = '';
                    e.classList.remove('incorrect');
                    e.classList.add('correct');


                }

            }
        })
    })
})